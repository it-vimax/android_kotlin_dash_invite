package com.it_vimax.dash_invite_lesson.Act

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fxn.pix.Pix
import com.fxn.utility.PermUtil
import com.it_vimax.dash_invite_lesson.R
import com.it_vimax.dash_invite_lesson.adapters.ImageAdapter
import com.it_vimax.dash_invite_lesson.databinding.ActivityEditAdsBinding
import com.it_vimax.dash_invite_lesson.dialogs.DialogSpinnerHelper
import com.it_vimax.dash_invite_lesson.frag.FragmentCloseInterface
import com.it_vimax.dash_invite_lesson.frag.ImageListFrag
import com.it_vimax.dash_invite_lesson.utils.CityHelper
import com.it_vimax.dash_invite_lesson.utils.ImagePicker


class EditAdsAct : AppCompatActivity(), FragmentCloseInterface {
    private var chooseImageFrag: ImageListFrag? = null
    lateinit var rootElement: ActivityEditAdsBinding
    private var dialog = DialogSpinnerHelper()
    private lateinit var imageAdapter: ImageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootElement = ActivityEditAdsBinding.inflate(layoutInflater)
        setContentView(rootElement.root)
        init()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK && requestCode == ImagePicker.REQUEST_CODE_GET_IMAGES) {
            if (data != null) {
                val returnValues = data.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                if (returnValues?.size!! > 1 && chooseImageFrag == null) {
                    openChooseImageFragment(returnValues)
                } else if (chooseImageFrag != null) {
                    chooseImageFrag?.updateAdapter(returnValues)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ImagePicker.getImages(this, ImagePicker.MAX_IMAGE_COUNT)
                } else {
                    Toast.makeText(
                        this,
                        "Approve permissions to open Pix ImagePicker",
                        Toast.LENGTH_LONG
                    ).show()
                }
                return
            }
        }
    }

    private fun init() {
        imageAdapter = ImageAdapter()
        rootElement.vpImages.adapter = imageAdapter
    }

    fun onClickSelectCountry(view: View) {
        val countryList = CityHelper.getAllCountries(this)
        dialog.showSpinnerDialog(this, countryList, rootElement.tvCounrty)
        if (rootElement.tvCity.text.toString() != getString(R.string.select_city)) {
            rootElement.tvCity.setText(getString(R.string.select_city))
        }
    }

    fun onClickSelectCity(view: View) {
        val selectedCountry = rootElement.tvCounrty.text.toString()
        if (selectedCountry == getString(R.string.select_country)) {
            Toast.makeText(this, getString(R.string.select_country), Toast.LENGTH_LONG).show()
        } else {
            val cityList = CityHelper.getAllCities(this, selectedCountry)
            dialog.showSpinnerDialog(this, cityList, rootElement.tvCity)
        }
    }

    fun onClickGetImages(view: View) {
        if (imageAdapter.mainArray.size < 1) {
            ImagePicker.getImages(this, ImagePicker.MAX_IMAGE_COUNT)
        } else {
            openChooseImageFragment(imageAdapter.mainArray)
        }
    }

    override fun onFragClose(list: ArrayList<String>) {
        rootElement.scrollViewMain.visibility = View.VISIBLE
        imageAdapter.update(list)
        chooseImageFrag = null
    }

    private fun openChooseImageFragment(newList: ArrayList<String>) {
        chooseImageFrag = ImageListFrag(this, newList)
        rootElement.scrollViewMain.visibility = View.GONE;
        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.place_holder, chooseImageFrag!!)
        fm.commit()
    }
}