package com.it_vimax.dash_invite_lesson.utils

import android.content.Context
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import kotlin.collections.ArrayList

object CityHelper {
    fun getAllCountries(context: Context): ArrayList<String> {
        var tempArray = ArrayList<String>()
        try {
            val inputStream: InputStream = context.assets.open("countriesToCities.json")
            val size: Int = inputStream.available()
            val bytesArray = ByteArray(size)
            inputStream.read(bytesArray)
            val jsonFile: String = String(bytesArray)
            val jsonObject = JSONObject(jsonFile)
            val countyNames = jsonObject.names()
            if (countyNames != null) {
                for (n in 0 until countyNames.length()) {
                    tempArray.add(countyNames.getString(n))
                }
            }
        } catch (e: IOException) {
        }

        return tempArray
    }

    fun getAllCities(context: Context, country: String): ArrayList<String> {
        var tempArray = ArrayList<String>()
        try {
            val inputStream: InputStream = context.assets.open("countriesToCities.json")
            val size: Int = inputStream.available()
            val bytesArray = ByteArray(size)
            inputStream.read(bytesArray)
            val jsonFile: String = String(bytesArray)
            val jsonObject = JSONObject(jsonFile)
            val sityNames = jsonObject.getJSONArray(country)

            for (n in 0 until sityNames.length()) {
                tempArray.add(sityNames.getString(n))
            }
        } catch (e: IOException) {
        }

        return tempArray
    }

    fun filterDataList(list: ArrayList<String>, searchText: String?): ArrayList<String> {
        val tempList = ArrayList<String>()
        tempList.clear()

        if (searchText == null) {
            tempList.add("not_found")
            return tempList
        }

        for (selection: String in list) {
            if (selection.lowercase().startsWith(searchText.lowercase())) {
                tempList.add(selection)
            }
        }

        if (tempList.size == 0) {
            tempList.add("not_found")
        }

        return tempList
    }
}