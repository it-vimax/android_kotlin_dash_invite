package com.it_vimax.dash_invite_lesson.frag

interface FragmentCloseInterface {
    fun onFragClose(list : ArrayList<String>)
}